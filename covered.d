module covered;

version(unittest) import beep;

import core.stdc.stdio;
import core.stdc.stdlib : free, exit;

version(unittest) { } else extern(C) @nogc int main(int argc, char** argv) {
	import core.stdc.string : strncpy, strcmp;

	if(argc == 1)
		argv[0].usage;

	for(size_t i = 1; i < argc; ++i) {
		if(!argv[i].strcmp("-h")) {
			argv[0].usage;
		} else if(!argv[i].strcmp("-a")) {
			float total = 0.0;
			size_t count;

			if(argc - i == 1)
				"At least 1 input file is expected".fail;

			for(size_t j = i + 1; j < argc; ++j) {
				auto file = fopen(argv[j], "r");
				if(file is null)
					"Could not open %s".fail(argv[j]);

				scope(exit) file.fclose;

				auto r = file.getCoverage;
				if(r != float.infinity) {
					total += r;
					++count;
				}
			}

			"Average: %.2f%%\n".printf(total / count);

			break;
		} else if(!argv[i].strcmp("-b")) {
			if(argc - i < 3)
				"At least 2 input files are expected".fail;

			for(size_t j = i + 1; j < argc; ++j) {
				auto file = fopen(argv[j], "r");
				if(file is null)
					"Could not open %s".fail(argv[j]);

				scope(exit) file.fclose;

				auto coverage = file.getCoverage;
				auto name = file.getSourceFile;
				scope(exit) free(cast(void*) name);

				coverage == float.infinity 
					? "%40s has no code\n".printf(name)
					: "%40s %.2f%%\n".printf(name, coverage);
			}
			break;
		} else {
			"Unknown argument '%s'\n".printf(argv[i]);
			argv[0].usage;
		}
	}

	return 0;
}

extern(C) @nogc void usage(char* program_name) {
	printf("Usage: %s <options> FILE...\n", program_name);

	exit(0);
}

extern(C) @nogc void fail(A...)(scope const(char)* fmt, A args) {
	stderr.fprintf(fmt, args);
	stderr.fprintf("\n");
	exit(-1);
}

extern(C) @nogc nothrow const(char)* getSourceFile(FILE* file) {
	import core.stdc.stdlib : malloc;
	import core.stdc.string : strrchr, strncpy;

	char[1024] buffer;

	file.fseek(0, SEEK_SET);

	while(fgets(buffer.ptr, buffer.length, file) !is null) {}

	auto end = buffer.ptr.strrchr('i'); // Line looks like `<filename> is <percent>% covered`

	if(end is null) { // It's possible that file has no code
		end = buffer.ptr.strrchr('h');

		if(end is null)
			"Could not find source file name. Invalid file?".fail;
	}
	
	auto r = cast(char*) malloc(end - buffer.ptr - 1);

	strncpy(r, buffer.ptr, end - buffer.ptr - 1);

	return r;
}

@("getSourceFile()")
unittest {
	import std.string : fromStringz;
	{
		auto file = fopen("test/hello.lst", "r");
		if(file is null)
			throw new Exception("Could not open file");

		scope(exit) file.fclose;

		auto p = file.getSourceFile;
		scope(exit) free(cast(void*) p);

		p.fromStringz.expect!equal("hello.d");
	}

	{
		auto file = fopen("test/empty.lst", "r");
		if(file is null)
			throw new Exception("Could not open file");

		scope(exit) file.fclose;

		auto p = file.getSourceFile;
		scope(exit) free(cast(void*) p);

		p.fromStringz.expect!equal("empty.d");
	}
}

extern(C) @nogc nothrow float getCoverage(FILE* file) {
	import core.stdc.stdlib : atoi;
	import core.stdc.string : strchr, strlen;

	char[4096] buffer;

	size_t covered;
	size_t total;

	file.fseek(0, SEEK_SET);

	while(fgets(buffer.ptr, buffer.length, file) !is null) {
		auto len = buffer.ptr.strlen;
		if(len < 8 || buffer.ptr.strchr('|') is null)
			continue;

		if(buffer[0] == '0') {
			++total;
		} else if(buffer.ptr.atoi != 0) {
			++total;
			++covered;
		}
	}

	if(total == 0)
		return float.infinity;

	return cast(float) covered / cast(float) total * 100.0f;
}

@("getCoverage()")
unittest {
	auto file = fopen("test/hello.lst", "r");
	if(file is null)
		throw new Exception("Could not open file");

	scope(exit) file.fclose;

	file.getCoverage.expect!equal(100.0f);
}

@("getCoverage() returns infinity on files that have no code")
unittest {
	auto file = fopen("test/empty.lst", "r");
	if(file is null)
		throw new Exception("Could not open file");

	scope(exit) file.fclose;

	file.getCoverage.expect!equal(float.infinity);
}
