covered [![Repository](https://img.shields.io/badge/repository-on%20GitLab-orange.svg)](https://gitlab.com/AntonMeep/covered) [![pipeline 
status](https://gitlab.com/AntonMeep/covered/badges/master/pipeline.svg)](https://gitlab.com/AntonMeep/covered/commits/master) [![coverage 
report](https://gitlab.com/AntonMeep/covered/badges/master/coverage.svg)](https://gitlab.com/AntonMeep/covered/commits/master) [![MIT 
Licence](https://img.shields.io/badge/licence-MIT-blue.svg)](https://gitlab.com/AntonMeep/covered/blob/master/LICENSE) [![Package 
version](https://img.shields.io/dub/v/covered.svg)](https://gitlab.com/AntonMeep/covered/tags)
=====

**covered** processes output of code coverage analysis performed by the D programming language compiler (DMD/LDC/GDC).
